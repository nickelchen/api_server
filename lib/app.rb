# called after the environment is setup and the basic dependencies are loaded
# but before the models are loaded.
#
# Here you want to load your dependencies and maybe set your
# datastore.


############### AIRBRAKE ###############
airbrake_config_file = File.join(WDSinatra::AppLoader.root_path, 'config', 'airbrake.yml')
if File.exist?(airbrake_config_file)
  airbrake_config = YAML.load_file(airbrake_config_file)[RACK_ENV]
  if airbrake_config && airbrake_config['enabled']
    require 'airbrake'
    Airbrake.configure do |config|
      config.api_key = airbrake_config['api_key']
      (airbrake_config['params_filters'] - config.params_filters).each do |param| 
        config.params_filters << param
      end
      config.environment_name = RACK_ENV
      config.host = airbrake_config['host'] if airbrake_config['host']
      config.logger = LOGGER
    end
  end
end


require 'wd_sinatra_active_record'
WdSinatraActiveRecord::DBConnector.set_db_connection
WdSinatraActiveRecord::DBConnector.connect_to_db unless ENV['DONT_CONNECT']



require 'pry'

helpers do
  def username
    session[:identity] ? session[:identity] : 'Hello stranger'
  end
end

before '/secure/*' do
  if !session[:identity] then
    session[:previous_url] = request.path
    @error = 'Sorry guacamole, you need to be logged in to visit ' + request.path
    halt erb(:login_form)
  end
end

get '/' do
  erb 'Can you handle a <a href="/secure/place">secret</a>?'
end

get '/login/form' do 
  erb :login_form
end

post '/login/attempt' do
  session[:identity] = params['username']
  where_user_came_from = session[:previous_url] || '/'
  redirect to where_user_came_from 
end

get '/logout' do
  session.delete(:identity)
  erb "<div class='alert alert-message'>Logged out</div>"
end


get '/secure/place' do
  erb "This is a secret place that only <%=session[:identity]%> has access to!"
end
