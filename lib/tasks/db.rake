require 'wd_sinatra_active_record'
load WdSinatraActiveRecord.task_path

namespace :db do
  desc "Create a migration(options:NAME=x, VERSION=x)."
  task :create_migration do
    require 'active_support/core_ext/string/strip'
    require 'fileutils'
    migration_name = ENV["NAME"]
    migration_version = ENV["VERSION"] || Time.now.utc.strftime("%Y%m%d%H%M%S")
    migrations_dir = ActiveRecord::Migrator.migrations_paths
    migration_file = File.join(migrations_dir, "#{migration_version}_#{migration_name}.rb")
    FileUtils.mkdir_p(migrations_dir)
    migration_class = migration_name.split('_').map(&:capitalize).join
    File.open(migration_file, 'w') do |file|
      file.write <<-MIGRATION.strip_heredoc
        class #{migration_class} < ActiveRecord::Migration
          def up
          end

          def down
          end
        end
      MIGRATION
    end
  end
end