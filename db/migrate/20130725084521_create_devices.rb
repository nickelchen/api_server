class CreateDevices < ActiveRecord::Migration
  def up
    create_table :devices do |t|
      t.column :name, :string
      t.column :area, :string
      t.column :mac, :string
    end
  end

  def down
    drop_table :devices
  end
end
