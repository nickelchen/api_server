class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.column :name, :string
      t.column :age, :integer
    end
  end

  def down
    drop_table :users
  end
end
